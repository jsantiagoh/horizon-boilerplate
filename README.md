Horizon boilerplate
===================

Based on https://github.com/flipace/lovli.js but removed everything that was not necessary for me.

Stack
-----

- [facebook/react](https://github.com/facebook/react) - View
- [reactjs/redux](https://github.com/reactjs/redux) - App State
- [rethinkdb/horizon](https://github.com/rethinkdb/horizon) - Realtime Database Sync
- [flipace/horizon-react](https://github.com/flipace/horizon-react) - Connect View + Data
- [gaearon/react-hot-loader](https://github.com/gaearon/react-hot-loader) - Hot Reloading of React Components
- [webpack/webpack](https://github.com/webpack/webpack) - Builds & Dev-Server

### Installation
``` bash
$ git clone git@gitlab.com:jsantiagoh/horizon-boilerplate.git
$ cd horizon-boilerplate
```

### Run
``` bash
$ docker-compose up --build
```
