FROM node:6-onbuild
EXPOSE 3000 9095 29015
CMD [ "npm", "run", "prod" ]
